# RegExTux

RegExTux is a free (as in freedom and beer) tool to perform regex testing for developers.
It is based on the QtRegularExpression framework, which in turn is based on the well-known [PCRE](https://pcre.org) engine.
Therefore, it supports all the features and syntax supproted by the PCRE engine.

To compile, download the sources and use the .pro file in the sources as a guide for settings. Note that you need Qt v5.10 library to compile (other versions might work, but I have not tested them).
If you wish to download the binaries, you can do so from [my website](https://maheshvk.com/software.html#ret).

This work is published under the [GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
