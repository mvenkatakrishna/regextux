/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * RegExTux : Program for testing regexes                                  *
 * Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)        *
 *                                                                         *
 * This file part of RegExTux which is free software: you can redistribute *
 * it and/or modify it under the terms of the GNU General Public License   *
 * as published by the Free Software Foundation, either version 3 of the   *
 * License, or (at your option) any later version.                         *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.                                                *
 * If not, see <https://www.gnu.org/licenses//gpl-3.0.en.html>.            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "ret_mainwin.h"
#include "ui_ret_mainwin.h"

#include <memory>

#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QTextCodec>
#include <QStandardPaths>
#include <QDateTime>

namespace
{
const QString head3 = "font-size: 14pt; color: rgb(250,100,100);";
const QString full = "font-size:12pt; color: rgb(100,100,250);";
const QString cap = "color: rgb(50,120,150);";
}

RET_MainWin::RET_MainWin(QWidget *parent)
    : QMainWindow(parent),
      m_ui(new Ui::RET_MainWin)
{
    m_ui->setupUi(this);

    m_gWin = std::make_unique<RET_GuideWin>();
    m_csWin = std::make_unique<RET_CheatsheetWin>();
    m_aWin = std::make_unique<RET_AboutWin>();

    m_re = QRegularExpression();
    m_re_match = QRegularExpressionMatch();
    m_re_match_it = QRegularExpressionMatchIterator();

    m_expTxt = "";
    m_inTxt = "";
    m_resTxt = "";

    m_i = false;
    m_s = false;
    m_m = false;
    m_g = true;
    m_u = false;
    m_x = false;
}

RET_MainWin::~RET_MainWin()
{
    delete m_ui;
}

void RET_MainWin::on_actionLoad_Text_From_File_triggered()
{
    QFile inFile( QFileDialog::getOpenFileName(nullptr, "Open File") );
    if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QByteArray content = inFile.readLine();
    QTextCodec * myCodec = QTextCodec::codecForUtfText(content);
    m_ui->inputEdit->setPlainText( myCodec->toUnicode( content.append(inFile.readAll()) ) );
    inFile.close();
}

void RET_MainWin::on_actionSave_Panes_To_File_triggered()
{
    QStringList tmp = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    QFile outFile( QFileDialog::getSaveFileName( nullptr, "Save File", (tmp.isEmpty()?"":tmp[0]), tr("Text files (*.txt)") ) );
    if(!outFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(nullptr, "Error", "Cannot save file! Please check permissions to the directory and that it exists...");
        return;
    }

    QString s = "RegExTux - Output from " + QDateTime::currentDateTime().toString("dd.MM.yyyy; hh:mm:ss") +
                "\n===========================================================================\n" +
                "Pattern: " + m_re.pattern() + "\nOptions: " +
                QString("i:%1, s:%2, m:%3, g:%4, u:%5, x:%6\n\n\n\n").arg(m_i).arg(m_s).arg(m_m).arg(m_g).arg(m_u).arg(m_x) +
                "Input text: \n" + m_ui->inputEdit->toPlainText() + "\n\n\n\n" +
                "Result: \n" + m_ui->resultOut->toPlainText() + "\n\n\n\n" +
                "Split: \n" + m_ui->splitOut->toPlainText() + "\n\n\n\n" +
                "Replace: \n" + "Text to replace: " + m_ui->replaceEdit->text() + "\n"+
                  "Result: \n" + m_ui->replaceOut->toPlainText() + "\n";
    outFile.write(s.toUtf8());
    outFile.close();
}

void RET_MainWin::on_actionExit_triggered()
{
    this->close();
}

void RET_MainWin::on_actionUser_Guide_triggered()
{
    m_gWin->show();
}

void RET_MainWin::on_actionRegEx_Cheat_Sheet_triggered()
{
    m_csWin->show();
}

void RET_MainWin::on_actionAbout_triggered()
{
    m_aWin->show();
}

void RET_MainWin::on_restBtn_clicked()
{
    m_setBoxStates(true);
    this->m_ui->regexEdit->clear();
    this->m_ui->inputEdit->clear();
    this->m_ui->resultOut->clear();
    this->m_ui->replaceEdit->clear();
    this->m_ui->replaceOut->clear();
}

void RET_MainWin::on_testBtn_clicked()
{
    m_expTxt = m_ui->regexEdit->text();
    QRegularExpression::PatternOptions flags = m_getFlags();

    m_re.setPattern(m_expTxt);
    m_re.setPatternOptions(flags);
    if (!m_re.isValid())
    {
        QMessageBox::critical(nullptr, "Invalid Pattern!", m_re.errorString());
        m_setBoxStates(false);
        return;
    }

    if (!m_g)
    {
        m_re_match = m_re.match(m_ui->inputEdit->toPlainText());
        if (m_re_match.lastCapturedIndex() < 0)
        {
            m_ui->resultOut->setHtml("<p style='"+head3+"'>No match found!</p>");
            return;
        }
        QStringList res = m_re_match.capturedTexts();
        QString res_txt = "<p style='"+head3+"'>Match:</p>"+
                          "<p>Full Match ["+QString("%1").arg(m_re_match.capturedStart(0))+"-"+
                          QString("%1").arg(m_re_match.capturedEnd(0))+"]: <span style='"+full+"'>"+
                          res[0].toHtmlEscaped()+"</span></p>";
        for (int i = 1; i < res.length(); ++i)
        {
            res_txt += "<p> Capturegroup "+QString("%1").arg(i)+" ["+
                       QString("%1").arg(m_re_match.capturedStart(i))+"-"+
                       QString("%1").arg(m_re_match.capturedEnd(i))+
                       "]: <span style='"+cap+"'>"+res[i].toHtmlEscaped()+"</span></p>";
        }
        m_ui->resultOut->setHtml(res_txt);
    }
    else
    {
        m_re_match_it = m_re.globalMatch(m_ui->inputEdit->toPlainText());
        QString res_txt = "";
        int cnt = 0;
        while(m_re_match_it.hasNext())
        {
            QRegularExpressionMatch match = m_re_match_it.next();
            QStringList res = match.capturedTexts();
            res_txt += "<p style='"+head3+"'>Match"+QString("%1").arg(++cnt)+":</p>"+
                       "<p>Full Match ["+QString("%1").arg(match.capturedStart(0))+"-"+
                       QString("%1").arg(match.capturedEnd(0))+"]: <span style='"+full+"'>"+
                       res[0].toHtmlEscaped()+"</span></p>";
            for (int i = 1; i < res.length(); ++i)
            {
                res_txt += "<p> Capturegroup "+QString("%1").arg(i)+" ["+
                        QString("%1").arg(match.capturedStart(i))+"-"+
                        QString("%1").arg(match.capturedEnd(i))+
                        "]: <span style='"+cap+"'>"+res[i].toHtmlEscaped()+"</span></p>";
            }
        }

        if(res_txt == "")
            res_txt = "<p style='"+head3+"'>No matches found!</p>";

        m_ui->resultOut->setHtml(res_txt);
    }
}

void RET_MainWin::on_splitBtn_clicked()
{
    if (!m_re.isValid())
    {
        QMessageBox::critical(nullptr, "Invalid Pattern!", m_re.errorString());
        //on_restBtn_clicked();
        m_setBoxStates(false);
        return;
    }

    QStringList split_res = m_ui->inputEdit->toPlainText().split(m_re);
    QString split_html = "";
    for( auto s : split_res)
    {
        split_html += "<p>"+s.toHtmlEscaped()+"</p>";
    }
    m_ui->splitOut->setHtml(split_html);
}

void RET_MainWin::on_replaceBtn_clicked()
{
    if (!m_re.isValid())
    {
        QMessageBox::critical(nullptr, "Invalid Pattern!", m_re.errorString());
        //on_restBtn_clicked();
        m_setBoxStates(false);
        return;
    }

    m_ui->replaceOut->setPlainText( m_ui->inputEdit->toPlainText().replace( m_re, m_ui->replaceEdit->text() ) );
}

inline QRegularExpression::PatternOptions RET_MainWin::m_getFlags()
{
    m_i = m_ui->iBox->isChecked();
    m_s = m_ui->sBox->isChecked();
    m_m = m_ui->mBox->isChecked();
    m_g = m_ui->gBox->isChecked();
    m_u = m_ui->uBox->isChecked();
    m_x = m_ui->xBox->isChecked();

    QRegularExpression::PatternOptions flags = QRegularExpression::NoPatternOption;

    if(m_i) flags |= QRegularExpression::CaseInsensitiveOption;
    if(m_s) flags |= QRegularExpression::DotMatchesEverythingOption;
    if(m_m) flags |= QRegularExpression::MultilineOption;
    if(m_u) flags |= QRegularExpression::UseUnicodePropertiesOption;
    if(m_x) flags |= QRegularExpression::ExtendedPatternSyntaxOption;

    return flags;
}

inline void RET_MainWin::m_setBoxStates(bool reset)
{
    this->m_ui->iBox->setCheckState(Qt::Unchecked);
    this->m_ui->sBox->setCheckState(Qt::Unchecked);
    this->m_ui->mBox->setCheckState(Qt::Unchecked);
    this->m_ui->gBox->setCheckState(Qt::Checked);
    this->m_ui->uBox->setCheckState(Qt::Unchecked);
    this->m_ui->xBox->setCheckState(Qt::Unchecked);

    if(!reset)
    {
        if (m_i) this->m_ui->iBox->setCheckState(Qt::Checked);
        if (m_s) this->m_ui->sBox->setCheckState(Qt::Checked);
        if (m_m) this->m_ui->mBox->setCheckState(Qt::Checked);
        if (!m_g) this->m_ui->gBox->setCheckState(Qt::Unchecked);
        if (m_u) this->m_ui->uBox->setCheckState(Qt::Checked);
        if (m_x) this->m_ui->xBox->setCheckState(Qt::Checked);
    }
}
