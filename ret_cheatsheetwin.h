/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * RegExTux : Program for testing regexes                                  *
 * Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)        *
 *                                                                         *
 * This file part of RegExTux which is free software: you can redistribute *
 * it and/or modify it under the terms of the GNU General Public License   *
 * as published by the Free Software Foundation, either version 3 of the   *
 * License, or (at your option) any later version.                         *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.                                                *
 * If not, see <https://www.gnu.org/licenses//gpl-3.0.en.html>.            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef RET_CHEATSHEETWIN_H
#define RET_CHEATSHEETWIN_H

#include <QWidget>

namespace Ui {
class RET_CheatsheetWin;
}

class RET_CheatsheetWin : public QWidget
{
    Q_OBJECT

public:
    explicit RET_CheatsheetWin(QWidget *parent = nullptr);
    ~RET_CheatsheetWin();

private:
    Ui::RET_CheatsheetWin *ui;
};

#endif // RET_CHEATSHEETWIN_H
