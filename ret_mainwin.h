/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * RegExTux : Program for testing regexes                                  *
 * Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)        *
 *                                                                         *
 * This file part of RegExTux which is free software: you can redistribute *
 * it and/or modify it under the terms of the GNU General Public License   *
 * as published by the Free Software Foundation, either version 3 of the   *
 * License, or (at your option) any later version.                         *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.                                                *
 * If not, see <https://www.gnu.org/licenses//gpl-3.0.en.html>.            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef RET_MAINWIN_H
#define RET_MAINWIN_H

#include <QMainWindow>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <memory>

#include "ret_guidewin.h"
#include "ret_cheatsheetwin.h"
#include "ret_aboutwin.h"

namespace Ui {
class RET_MainWin;
}

class RET_MainWin : public QMainWindow
{
    Q_OBJECT

public:
    RET_MainWin(QWidget *parent = nullptr);
    ~RET_MainWin();

private slots:
    // Handle Load Text From File action
    void on_actionLoad_Text_From_File_triggered();

    // Handle Save Text To File action
    void on_actionSave_Panes_To_File_triggered();

    // Handle Exit action
    void on_actionExit_triggered();

    // Handle User Guide action
    void on_actionUser_Guide_triggered();

    // Handle Regex Cheat Sheet action
    void on_actionRegEx_Cheat_Sheet_triggered();

    // Handle About action
    void on_actionAbout_triggered();

    // Handle Reset button click
    void on_restBtn_clicked();

    // Handle Test button click
    void on_testBtn_clicked();

    // Handle Replace button click
    void on_replaceBtn_clicked();

    // Handle Split button click
    void on_splitBtn_clicked();

private:
    // Function to get the flags that are set in the GUI and store them in internal members.
    inline QRegularExpression::PatternOptions m_getFlags();

    // Function to set the box states to match the internal members or to reset them to default states.
    inline void m_setBoxStates(bool reset);

    Ui::RET_MainWin *m_ui;

    std::unique_ptr<RET_GuideWin> m_gWin;
    std::unique_ptr<RET_CheatsheetWin> m_csWin;
    std::unique_ptr<RET_AboutWin> m_aWin;

    QRegularExpression m_re;
    QRegularExpressionMatch m_re_match;
    QRegularExpressionMatchIterator m_re_match_it;

    QString m_expTxt;
    QString m_inTxt;
    QString m_resTxt;

    bool m_i;
    bool m_s;
    bool m_m;
    bool m_g;
    bool m_u;
    bool m_x;
};

#endif // RET_MAINWIN_H
